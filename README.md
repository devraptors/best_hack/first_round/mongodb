# How to configure MongoDB

## 1) Docker-compose
```(yml)
version: '3.8'

services:
    mongodb:
        container_name: mongo-dev
        image: mongo
        environment:
            - MONGO_INITDB_ROOT_USERNAME=admin
            - MONGO_INITDB_DATABASE=bot
            - MONGO_INITDB_ROOT_PASSWORD=pass
        ports:
            - 27017:27017
    mongo-express:
        container_name: mongo-express
        image: mongo-express 
        depends_on: 
            - mongodb 
        environment: 
            - ME_CONFIG_MONGODB_ADMINUSERNAME=admin 
            - ME_CONFIG_MONGODB_ADMINPASSWORD=pass 
            - ME_CONFIG_MONGODB_SERVER=mongo-dev 
            - ME_CONFIG_BASICAUTH_USERNAME=admin 
            - ME_CONFIG_BASICAUTH_PASSWORD=longpassword 
        ports: 
            - 8081:8081  
```
## 2) Setup database

```bash
> mongo 
> use admin
> db.auth(admin, pass)
> use bot
> db.createUser(
  {
    user: "bot",
    pwd: "passw0rd",
    roles: [ { role: "readWrite", db: "bot" } ],
    passwordDigestor:"server"
  }
)
> db.counters.insert(
   {
      _id: "userid",
      seq: 1
   }
)
```
