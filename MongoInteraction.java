package modules.mongodb;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mongodb.*;

import javax.swing.text.Document;

public class MongoInteraction {
    public static final class MongoConfig {
        public final String host;
        public final int port;
        public final String db;
        public final String username;
        public final String password;

        public MongoConfig(String host, int port, String db, String username, String password) {
            this.host = host;
            this.port = port;
            this.db = db;
            this.username = username;
            this.password = password;
        }
    }

    public static class MongoData {
        public final String username;
        public final String chat_id;
        public final String country;
        public MongoData(String username, String chat_id, String country){
            this.chat_id = chat_id;
            this.username = username;
            this.country = country;
        }
    }

    public MongoCredential credential;
    public DB database;
    public MongoClient client;

    public MongoInteraction(MongoConfig config) {
        credential = MongoCredential.createCredential(config.username, config.db, config.password.toCharArray());
        client = new MongoClient(new ServerAddress(config.host, config.port), Arrays.asList(credential));
        database = client.getDB(config.db);
    }

    public void sendData(String service, MongoData data, boolean isFrom) {
        DBCollection collection = database.getCollection(service);
        BasicDBObject searchQuery = new BasicDBObject("username", data.username);
        DBCursor cursor = collection.find(searchQuery);
        if (cursor.count() != 0) {
            BasicDBObject updateFields = new BasicDBObject();
            if (isFrom) {
                updateFields.append("from", data.country);
            } else {
                List<String> array = (List<String>) cursor.next().get("to");
                if (array == null) {
                    array = new ArrayList<>();
                }
                array.add(data.country);
                updateFields.append("to", array);
            }

            BasicDBObject setQuery = new BasicDBObject();
            setQuery.append("$set", updateFields);
            collection.update(searchQuery, setQuery);
            return;
        }

        DBObject mongo_data;

        if (isFrom) {
            mongo_data = new BasicDBObject("_id", getNextSequence("userid"))
                    .append("username", data.username)
                    .append("chat_id", data.chat_id)
                    .append("from", data.country);
        } else {
            List<String> array = new ArrayList<>();
            array.add(data.country);
            mongo_data = new BasicDBObject("_id", getNextSequence("userid"))
                    .append("username", data.username)
                    .append("chat_id", data.chat_id)
                    .append("to", array);
        }
        collection.insert(mongo_data);
    }

    public String getNextSequence(String name) {
        DBCollection collection = database.getCollection("counters");
        BasicDBObject find = new BasicDBObject();
        find.put("_id", name);
        BasicDBObject update = new BasicDBObject();
        update.put("$inc", new BasicDBObject("seq", 1));
        DBObject obj = collection.findAndModify(find, update);
        return obj.get("seq").toString();
    }

    public DBCursor getAllValues(String service) {
        DBCollection collection = database.getCollection(service);
        return collection.find();
    }
}
